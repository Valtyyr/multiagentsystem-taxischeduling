package agents;

import java.awt.Color;

import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;

public class TaxiAgentStyle extends DefaultStyleOGL2D {

	@Override
	public Color getColor(Object o) {
		if (o instanceof TaxiAgent) {
			if (((TaxiAgent) o).getClientAgentsInTaxi().size() != 0) {
				return Color.BLUE;
			}
		}
		return Color.YELLOW;
	}
}
