package agents;

import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import environment.AEnvironment;

public abstract class AAgent extends AEnvironment {
	
	protected final Network<AEnvironment> mNetwork;
	protected final Grid<AEnvironment> mGrid;

	public AAgent(Network<AEnvironment> iNetwork, Grid<AEnvironment> iGrid) {
		mNetwork = iNetwork;
		mGrid = iGrid;
	}
	
	@ScheduledMethod(start = 1, interval = 1, priority = 2)
	public abstract void compute();

	@ScheduledMethod(start = 1, interval = 1, priority = 1)
	public abstract void implement();
}
