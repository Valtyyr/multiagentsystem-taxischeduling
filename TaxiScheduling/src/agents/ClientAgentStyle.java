package agents;

import java.awt.Color;

import data.Type.BooleanClientType;
import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;

public class ClientAgentStyle extends DefaultStyleOGL2D {

	@Override
	public Color getColor(Object o) {
		if (o instanceof ClientAgent) {
			if (((ClientAgent) o).getBooleanMap().get(BooleanClientType.inTaxi)) {
				return Color.GREEN;
			}
		}
		return Color.ORANGE;
	}
}
