package agents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import repast.simphony.context.Context;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;
import taxi.ContextCreator;
import data.DataBase;
import data.Type.BooleanClientType;
import data.Type.IntegerClientType;
import data.Type.IntegerTaxiType;
import data.Type.NodeClientType;
import environment.AEnvironment;
import environment.Node;

public class TaxiAgent extends AAgent {
	
	private final Map<IntegerTaxiType, Integer> mIntegerMap;
	private final List<ClientAgent> mClientAgentsInTaxi;
	private Stack<Node> mPathToObjective;
	private Node mNextNode;
	private boolean mBackHome;

	public ClientAgent mAimedClient;
	public Node mCurrentNode;
	
	public TaxiAgent(Network<AEnvironment> iNetwork, Grid<AEnvironment> iGrid) {
		super(iNetwork, iGrid);
		mIntegerMap = new HashMap<IntegerTaxiType, Integer>();
		mClientAgentsInTaxi = new ArrayList<ClientAgent>();
		mPathToObjective = new Stack<Node>();
		mCurrentNode = DataBase.getInstance().getTaxiBase();
		mIntegerMap.put(IntegerTaxiType.nbClients, 0);
		mIntegerMap.put(IntegerTaxiType.nbMaxClients, 0);
		mAimedClient = null;
		mBackHome = false;
	}

	public List<ClientAgent> getClientAgentsInTaxi() {
		return mClientAgentsInTaxi;
	}

	public Map<IntegerTaxiType, Integer> getIntegerMap() {
		return mIntegerMap;
	}
	
	/*
	 * Update the next node
	 */
	@Override
	public void compute() {
		if (!mPathToObjective.empty()) {
			mNextNode = mPathToObjective.pop();
		}
		
		/*
		 * The tour is finish
		 */
		if (mBackHome && mClientAgentsInTaxi.isEmpty()
				&& mAimedClient == null
				&& mCurrentNode.equals(mNextNode)
				&& mCurrentNode.equals(DataBase.getInstance().getTaxiBase())) {
			DataBase.getInstance().removeTaxiAgent(this);
		}
	}

	/*
	 * Move to the next node
	 * Check if it's an interesting node
	 */
	@Override
	public void implement() {
		if (mNextNode != null/* && mCanGo*/) {
			mCurrentNode = mNextNode;
			move();
			if (mPathToObjective.empty()) {
				processInterestingNode();
			}
		}
	}

	/*
	 * Consider a new Client
	 * Build a path to the begin node
	 */
	public void considerClientAgent(ClientAgent iClientAgent) {
		mAimedClient = iClientAgent;
		mAimedClient.getBooleanMap().put(BooleanClientType.aimed, true);
		buildPathTo(mAimedClient.getNodeMap().get(NodeClientType.beginNode));
	}

	/*
	 * Drop Clients if it's their endNode
	 * Pick up Client if it's their beginNode
	 */
	private void processInterestingNode() {
		/*
		 * Take the Client if the Taxi is
		 * on a beginNode
		 */
		if (mAimedClient != null
				&& !mAimedClient.getBooleanMap().get(BooleanClientType.inTaxi)
				&& !mClientAgentsInTaxi.contains(mAimedClient)
				&& mCurrentNode.equals(mAimedClient.getNodeMap().get(NodeClientType.beginNode))) {
			if (ContextCreator.sDebug) {System.out.println("TAXI : It's currently "
					+ DataBase.getInstance().getCurrentTime());}
			if (ContextCreator.sDebug) {System.out.println("TAXI : Must arrive after "
					+ mAimedClient.getIntegerMap().get(IntegerClientType.beginDate));}
			if (ContextCreator.sDebug) {System.out.println("TAXI : Must arrive before "
					+ mAimedClient.getIntegerMap().get(IntegerClientType.endDate));}
			arriveOnClientStart(mAimedClient);
		}

		/*
		 * Need of new List to stock Client(s) to remove
		 * e.g Client(s) whose the current Node it's their endNode
		 * New List used because of concurrent access matter
		 */
		List<ClientAgent> lAgentToRemove = new ArrayList<ClientAgent>();

		/*
		 * Stock Clients in the List if the Taxi
		 * is on a endNode
		 */
		for (ClientAgent lClientAgent : mClientAgentsInTaxi) {
			if (lClientAgent.getBooleanMap().get(BooleanClientType.inTaxi)
					&& mCurrentNode.equals(lClientAgent.getNodeMap().get(NodeClientType.endNode))) {
				lAgentToRemove.add(lClientAgent);
			}
		}

		/*
		 * Drop all Clients in the List
		 */
		for (ClientAgent lClientAgent : lAgentToRemove) {
			if (ContextCreator.sDebug) {System.out.println("TAXI : It's currently "
					+ DataBase.getInstance().getCurrentTime());}
			if (ContextCreator.sDebug) {System.out.println("TAXI : Must arrive at "
					+ lClientAgent.getIntegerMap().get(IntegerClientType.endDate));}
			arriveOnClientDestination(lClientAgent);
		}

		if (!lAgentToRemove.isEmpty() && mClientAgentsInTaxi.isEmpty()) {
			mAimedClient = null;
		}

		lAgentToRemove.clear();

		if (!mClientAgentsInTaxi.isEmpty()) {
			mPathToObjective.clear();
			mPathToObjective = DataBase.getInstance().newOptimizedPath(mCurrentNode, mClientAgentsInTaxi, this);
			if (ContextCreator.sDebug) {System.out.println("TAXI : Has " + mClientAgentsInTaxi.size() + " client(s) inside");}
		} else if (mAimedClient == null) {
			backHome();
		}
	}

	/*
	 * Pick up the Client
	 */
	private void arriveOnClientStart(ClientAgent iClientAgent) {
		if (DataBase.getInstance().getCurrentTime() >= iClientAgent.getIntegerMap().get(IntegerClientType.beginDate)) {
			mClientAgentsInTaxi.add(iClientAgent);
			int lClientAccu = mIntegerMap.get(IntegerTaxiType.nbClients);
			mIntegerMap.put(IntegerTaxiType.nbClients, lClientAccu + 1);
			int lClientInTaxi = mClientAgentsInTaxi.size();
			if (lClientInTaxi > mIntegerMap.get(IntegerTaxiType.nbMaxClients)) {
				mIntegerMap.put(IntegerTaxiType.nbMaxClients, lClientInTaxi);
			}
			iClientAgent.getNodeMap().put(NodeClientType.pickupNode, mCurrentNode);
			iClientAgent.inTaxi();
		}
	}

	/*
	 * Take the money of the Client
	 * Drop the Client
	 */
	private void arriveOnClientDestination(ClientAgent iClientAgent) {
		if (DataBase.getInstance().getCurrentTime() <= iClientAgent.getIntegerMap().get(IntegerClientType.endDate) + ContextCreator.sTimeService) {
			mClientAgentsInTaxi.remove(iClientAgent);
			iClientAgent.getNodeMap().put(NodeClientType.dropNode, mCurrentNode);
			iClientAgent.outTaxi();
		}
	}

	/*
	 * Taxi build a path to 
	 * the Taxi Base
	 */
	private void backHome() {
		mBackHome = true;
		mClientAgentsInTaxi.clear();
		buildPathTo(DataBase.getInstance().getTaxiBase());
		if (ContextCreator.sDebug) {System.out.println("TAXI : Is backing home");}
	}

	/*
	 * Establish the new path to the destination
	 * Dijkstra path-finding
	 * Set the node list inside a node stack
	 */
	private void buildPathTo(Node iDestinationNode) {
		if (ContextCreator.sDebug) {System.out.println("TAXI : Create new path to : " + iDestinationNode.getPlaceName());}
		mPathToObjective.clear();
		List<Node> lNodes = DataBase.getInstance().pathFinding(mCurrentNode, iDestinationNode);
		mPathToObjective.add(iDestinationNode);
		mPathToObjective.addAll(lNodes);
	}

	/*
	 * Move the Taxi and
	 * all Clients inside
	 */
	@SuppressWarnings("unchecked")
	private void move() {
		GridPoint lPointNode = mGrid.getLocation(mCurrentNode);
		mGrid.moveTo(this, lPointNode.getX(), lPointNode.getY());
		Context<AEnvironment> lContext = ContextUtils.getContext(this);
		for (ClientAgent lClientAgent : mClientAgentsInTaxi) {
			if (lContext.contains(lClientAgent)) {
				mGrid.moveTo(lClientAgent, lPointNode.getX(), lPointNode.getY());
			}
		}
	}
}
