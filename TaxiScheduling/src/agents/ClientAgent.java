package agents;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import taxi.ContextCreator;
import data.DataBase;
import data.Type.BooleanClientType;
import data.Type.BooleanNodeType;
import data.Type.IntegerClientType;
import data.Type.NodeClientType;
import environment.AEnvironment;
import environment.Node;

public class ClientAgent extends AAgent {

	private final Map<BooleanClientType, Boolean> mBooleanMap;
	private final Map<NodeClientType, Node> mNodeMap;
	private final Map<IntegerClientType, Integer> mIntegerMap;
	
	public ClientAgent(Network<AEnvironment> iNetwork, Grid<AEnvironment> iGrid) {
		super(iNetwork, iGrid);
		mBooleanMap = new HashMap<BooleanClientType, Boolean>();
		mIntegerMap = new HashMap<IntegerClientType, Integer>();
		mNodeMap = new HashMap<NodeClientType, Node>();
		
		mBooleanMap.put(BooleanClientType.inTaxi, false);
		mBooleanMap.put(BooleanClientType.aimed, false);
		
		mIntegerMap.put(IntegerClientType.beginDate, 0);
		mIntegerMap.put(IntegerClientType.endDate, Integer.MAX_VALUE);
	}
	
	public Map<BooleanClientType, Boolean> getBooleanMap() {
		return mBooleanMap;
	}

	public Map<NodeClientType, Node> getNodeMap() {
		return mNodeMap;
	}

	public Map<IntegerClientType, Integer> getIntegerMap() {
		return mIntegerMap;
	}
	
	@Override
	public void compute() {}

	@Override
	public void implement() {
		int lDurationBaseToBegin = 
				DataBase.getInstance().calculateDuration(DataBase.getInstance().pathFinding(DataBase.getInstance().getTaxiBase(), 
						                                                                    mNodeMap.get(NodeClientType.beginNode)));
		
		int lDurationBeginToEnd = 
				DataBase.getInstance().calculateDuration(DataBase.getInstance().pathFinding(mNodeMap.get(NodeClientType.beginNode), 
                        																	mNodeMap.get(NodeClientType.endNode)));
		
		int lCurrentTime = DataBase.getInstance().getCurrentTime();
		
		/*
		 * The time is running!
		 * Client is calling a Taxi for itself
		 */
		if (lCurrentTime >= mIntegerMap.get(IntegerClientType.beginDate)
				&& lCurrentTime + lDurationBaseToBegin + lDurationBeginToEnd + 5 >= mIntegerMap.get(IntegerClientType.endDate)
				&& isDisponible()) {
			if (ContextCreator.sDebug) {System.out.println("CLIENT : Wants taxi : "
					+ mNodeMap.get(NodeClientType.beginNode).getPlaceName()
					+ " --------> "
					+ mNodeMap.get(NodeClientType.endNode).getPlaceName());}
			
			/* Begin Method 3 */
			if (ContextCreator.sVersion3) {
				DataBase.getInstance().spawClientAgent(this);
			}
			/* End Method 3 */
			
			if (isDisponible()) {
				DataBase.getInstance().callTaxi(this);
			}
		}

		/*
		 * Debug for Failure
		 */
		if (DataBase.getInstance().getCurrentTime() > mIntegerMap.get(IntegerClientType.endDate) + ContextCreator.sTimeService * 2
				&& mBooleanMap.get(BooleanClientType.inTaxi)) {
			System.err.println("CLIENT FAILURE : Drop client at time : "
				+ mIntegerMap.get(IntegerClientType.endDate));
		} else if (DataBase.getInstance().getCurrentTime() > mIntegerMap.get(IntegerClientType.endDate)
				&& !mBooleanMap.get(BooleanClientType.inTaxi)) {
			System.err.println("CLIENT FAILURE : Pick up client at time, time out at :	"
				+ mIntegerMap.get(IntegerClientType.endDate));
			
			if (mBooleanMap.get(BooleanClientType.aimed)) {
				System.err.println("CLIENT FAILURE : Is aimed by a taxi");
			}
		}
	}

	/*
	 * Client get in the Taxi
	 */
	public void inTaxi() {
		mBooleanMap.put(BooleanClientType.inTaxi, true);
		mBooleanMap.put(BooleanClientType.aimed, false);
		mIntegerMap.put(IntegerClientType.pickupDate, DataBase.getInstance().getCurrentTime());
		if (ContextCreator.sDebug) {System.out.println("CLIENT : Is in taxi");}
	}

	/*
	 * Client get out from the Taxi
	 */
	public void outTaxi() {
		mBooleanMap.put(BooleanClientType.inTaxi, false);
		mNodeMap.get(NodeClientType.endNode).getBooleanMap().put(BooleanNodeType.isDest, false);
		mIntegerMap.put(IntegerClientType.dropDate, DataBase.getInstance().getCurrentTime());
		if (ContextCreator.sDebug) {System.out.println("CLIENT : Is arrived");}
		DataBase.getInstance().removeClientAgent(this);
	}
	
	/*
	 * Client is waiting for a Taxi
	 */
	public boolean isDisponible() {
		return !mBooleanMap.get(BooleanClientType.aimed) && !mBooleanMap.get(BooleanClientType.inTaxi);
	}
	
	public void init() {
		initNodes();
		initDates();
	}

	/*
	 * Choose randomly two nodes in the graph
	 */
	private void initNodes() {
		List<Node> lNodes = DataBase.getInstance().getGraph().getNodes();
		Node lBeginNode = lNodes.get((int) (Math.random() * 100) % lNodes.size());
		Node lEndNode = lNodes.get((int) (Math.random() * 100) % lNodes.size());

		mNodeMap.put(NodeClientType.beginNode, lBeginNode);

		while (lEndNode.equals(lBeginNode)) {
			lEndNode = lNodes.get((int) (Math.random() * 100) % lNodes.size());
			if (ContextCreator.sDebug) {System.out.println("CLIENT : Is looking for new end node");}
		}
		lEndNode.getBooleanMap().put(BooleanNodeType.isDest, true);
		mNodeMap.put(NodeClientType.endNode, lEndNode);
		
		if (ContextCreator.sDebug) {System.out.println("CLIENT : Spawn nodes "
		+ lBeginNode.getPlaceName()
		+ " ---------> " + lEndNode.getPlaceName());}
	}

	/*
	 * Choose randomly two dates
	 * Those dates must be compatible with the source and destination duration
	 */
	private void initDates() {
		int lDurationToBegin = 
				DataBase.getInstance().calculateDuration(DataBase.getInstance().pathFinding(DataBase.getInstance().getTaxiBase(), 
						                                                                    mNodeMap.get(NodeClientType.beginNode)));
		
		int lDuration = DataBase.getInstance().calculateDuration(DataBase.getInstance().pathFinding(mNodeMap.get(NodeClientType.beginNode),
																									mNodeMap.get(NodeClientType.endNode)));
		int lTickCount = DataBase.getInstance().getCurrentTime();
		int lBeginDate = lTickCount + (int) (Math.random() * 100) + lDurationToBegin + 10;
		int lEndDate = lBeginDate + lDuration + (int) (Math.random() * 200); 
		
		mIntegerMap.put(IntegerClientType.beginDate, lBeginDate);
		mIntegerMap.put(IntegerClientType.endDate, lEndDate);
		
		if (ContextCreator.sDebug) {System.out.println("CLIENT : Spawn dates "
		+ lBeginDate + " ---------> " + lEndDate);}
	}
}
