package data;

public class Type {

	public static enum BooleanClientType {
		inTaxi,
		aimed;
	}
	
	public static enum IntegerClientType {
		spawnDate,
		beginDate,
		pickupDate,
		dropDate,
		endDate;
	}
	
	public static enum NodeClientType {
		beginNode,
		endNode,
		pickupNode,
		dropNode;
	}
	
	public static enum BooleanNodeType {
		isDest;
	}
	
	public static enum IntegerTaxiType {
		spawnDate,
		backhomeDate,
		nbClients,
		nbMaxClients;
	}
}
