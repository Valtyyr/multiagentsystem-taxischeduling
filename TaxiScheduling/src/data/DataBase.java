package data;

import io.FileTools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.space.graph.Network;
import repast.simphony.space.graph.RepastEdge;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import taxi.ContextCreator;
import agents.ClientAgent;
import agents.TaxiAgent;
import data.Type.BooleanNodeType;
import data.Type.IntegerClientType;
import data.Type.IntegerTaxiType;
import data.Type.NodeClientType;
import environment.AEnvironment;
import environment.Graph;
import environment.Node;

public class DataBase {

	/*
	 * The DataBase is a Singleton with the whole data
	 * Handle all complex calculus
	 */
	private static DataBase sInstance;
	
	private final Graph mGraph;
	private final List<TaxiAgent> mTaxiAgents;
	private final List<ClientAgent> mClientAgents;
	private final Map<ClientAgent, Integer> mClientAgentsInStock;
	private Context<AEnvironment> mContext;
	private Network<AEnvironment> mNetwork;
	private Grid<AEnvironment> mGrid;
	private Node mTaxiBase;
	
	private DataBase() {
		mGraph = new Graph();
		mTaxiAgents = new ArrayList<TaxiAgent>();
		mClientAgents = new ArrayList<ClientAgent>();
		mClientAgentsInStock = new HashMap<ClientAgent, Integer>();
	}
	
	public static DataBase getInstance() {
		return sInstance == null ? sInstance = new DataBase() : sInstance;
	}

	public void init(Context<AEnvironment> iContext, Grid<AEnvironment> iGrid,
			Network<AEnvironment> iNetwork) {
		mContext = iContext;
		mGrid = iGrid;
		mNetwork = iNetwork;
	}
	
	public int getCurrentTime() {
		return (int) RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
	}

	public Graph getGraph() {
		return mGraph;
	}

	public List<TaxiAgent> getTaxiAgents() {
		return mTaxiAgents;
	}

	public List<ClientAgent> getClientAgents() {
		return mClientAgents;
	}

	public Map<ClientAgent, Integer> getClientAgentsInStock() {
		return mClientAgentsInStock;
	}
	
	public Node getTaxiBase() {
		return mTaxiBase;
	}

	public void setTaxiBase(Node iTaxiBase) {
		mTaxiBase = iTaxiBase;
		if (ContextCreator.sDebug) {System.out.println("INPUT : Taxi base is : " + iTaxiBase.getPlaceName());}
	}

	/*
	 * Looking for best path in order
	 * to optimized the Client collect
	 */
	public Stack<Node> newOptimizedPath(Node iCurrentTaxiNode, List<ClientAgent> iClientsInTaxi, TaxiAgent iTaxi) {
		return mGraph.newOptimizedPath(iCurrentTaxiNode, iClientsInTaxi, mClientAgents, getCurrentTime(), iTaxi);
	}

	/*
	 * Calculate the shortest path between Source and Destination
	 */
	public List<Node> pathFinding(Node iSourceNode, Node iDestinationNode) {
		return mGraph.getDijkstra(iSourceNode, iDestinationNode);
	}

	/*
	 * Calculate the duration of the input Path
	 */
	public int calculateDuration(List<Node> iPath) {
		return mGraph.calculateDuration(iPath);
	}
		
	/*
	 * Send a message for every Taxi that there 
	 * is a new Client ready to pick up
	 */
	public void spawClientAgent(ClientAgent iClientAgent) {
		for (TaxiAgent lTaxi : mTaxiAgents) {
			if (lTaxi.getClientAgentsInTaxi().isEmpty() && lTaxi.mAimedClient == null) {
				lTaxi.considerClientAgent(iClientAgent);
			}
		}
	}

	/*
	 * Call a taxi with a Client priority
	 */
	public void callTaxi(ClientAgent iClientAgent) {
		TaxiAgent lTaxi = new TaxiAgent(mNetwork, mGrid);
		lTaxi.considerClientAgent(iClientAgent);
		mTaxiAgents.add(lTaxi);
		mContext.add(lTaxi);
		lTaxi.getIntegerMap().put(IntegerTaxiType.spawnDate, DataBase.getInstance().getCurrentTime());
		GridPoint lPointNode = mGrid.getLocation(mTaxiBase);
		mGrid.moveTo(lTaxi, lPointNode.getX(), lPointNode.getY());
	}

	/*
	 * Create new Client in the Context
	 */
	public void createNewRandomClient() {
		if (mClientAgents.size() <= ContextCreator.sNbMaxClientInApp) {
			ClientAgent lClient = new ClientAgent(mNetwork, mGrid);
			lClient.init();
			addClientInContext(lClient);
		}
	}

	public void addEdge(String iSourceNodeName, String iDestinationNodeName, int iWeight) {
		Node lSourceNode = null; 
		Node lDestinationNode = null;

		for (Node lNode : mGraph.getNodes()) {
			if (lNode.getPlaceName().equals(iSourceNodeName)) {
				lSourceNode = lNode;
			} else if (lNode.getPlaceName().equals(iDestinationNodeName)) {
				lDestinationNode = lNode;
			}
		}
		if (lSourceNode == null) {System.err.println("INPUT : Source Node is not define");}
		if (lDestinationNode == null) {System.err.println("INPUT : Destination Node is not define");}
		
		if (lSourceNode != null && lDestinationNode != null) {
			addEdge(lSourceNode, lDestinationNode, iWeight);
		}
	}

	public void addEdge(Node iSourceNode, Node iDestinationNode, int iWeight) {
		if (ContextCreator.sDebug) {System.out.println("INPUT : Add new edge : " 
				+ iSourceNode.getPlaceName() + " <---" + iWeight + "---> " + iDestinationNode.getPlaceName());}
		mNetwork.addEdge(new RepastEdge<AEnvironment>(iSourceNode, iDestinationNode, false, iWeight));
		iSourceNode.addAdjacentNode(iDestinationNode, iWeight);
		iDestinationNode.addAdjacentNode(iSourceNode, iWeight);
	}
	
	public void addNode(String iNodeName, int iX, int iY) {
		Node lNode = new Node(iNodeName);
		addNode(lNode, iX, iY);
	}

	public void addNode(Node iNode, int iX, int iY) {
		if (ContextCreator.sDebug) {System.out.println("INPUT : Add new node : "  + iNode.getPlaceName());}
		mContext.add(iNode);
		mGrid.moveTo(iNode, iX, iY);
		mGraph.getNodes().add(iNode);
	}

	/*
	 * Add new Client in the Stock
	 */
	public void addClientInStock(int iSpawnDate, int iBeginDate, int iEndDate, String iBeginNodeName, String iEndNodeName) {
		ClientAgent lCientAgent = new ClientAgent(mNetwork, mGrid);
		lCientAgent.getIntegerMap().put(IntegerClientType.beginDate, iBeginDate);
		lCientAgent.getIntegerMap().put(IntegerClientType.endDate, iEndDate);
		Node lBeginNode = null; 
		Node lEndNode = null;
		
		for (Node lNode : mGraph.getNodes()) {
			if (lNode.getPlaceName().equals(iBeginNodeName)) {
				lBeginNode = lNode;
			} else if (lNode.getPlaceName().equals(iEndNodeName)) {
				lEndNode = lNode;
			}
		}
		if (lBeginNode == null) {System.err.println("INPUT : Begin Node is not define");}
		if (lEndNode == null) {System.err.println("INPUT : End Node is not define");}

		if (lBeginNode != null && lEndNode != null) {
			lCientAgent.getNodeMap().put(NodeClientType.beginNode, lBeginNode);
			lCientAgent.getNodeMap().put(NodeClientType.endNode, lEndNode);
			mClientAgentsInStock.put(lCientAgent, iSpawnDate);
		}
	}

	/*
	 * Check if it's time for Clients to spawn
	 */
	public void updateClientInContextSchedule() {
		for (Entry<ClientAgent, Integer> lEntry : mClientAgentsInStock.entrySet()) {
			if (getCurrentTime() == lEntry.getValue()) {
				addClientInContext(lEntry.getKey());
				mClientAgentsInStock.remove(lEntry);
			}
		}
	}

	/*
	 * Random generation of Client
	 */
	public void updateClientInContextRandom() {
		if (getCurrentTime() == 1) {
			while (mClientAgents.size() < 20) {
				createNewRandomClient();
			}
		}
		
		if (Math.random() > 0.69) {
			createNewRandomClient();
		}
	}
	
	/*
	 * Add the Client in the context and data
	 * Check if it's possible to consider this Client
	 */
	public void addClientInContext(ClientAgent iClientAgent) {
		int lDuration = calculateDuration(pathFinding(iClientAgent.getNodeMap().get(NodeClientType.beginNode),
													  iClientAgent.getNodeMap().get(NodeClientType.endNode)));
		if (iClientAgent.getIntegerMap().get(IntegerClientType.beginDate) + lDuration
				< iClientAgent.getIntegerMap().get(IntegerClientType.endDate) - ContextCreator.sTimeService * 2) {
			mClientAgents.add(iClientAgent);
			mContext.add(iClientAgent);
			iClientAgent.getNodeMap().get(NodeClientType.endNode).getBooleanMap().put(BooleanNodeType.isDest, true);
			iClientAgent.getIntegerMap().put(IntegerClientType.spawnDate, getCurrentTime());
			GridPoint lPointNode = mGrid.getLocation(iClientAgent.getNodeMap().get(NodeClientType.beginNode));
			mGrid.moveTo(iClientAgent, lPointNode.getX(), lPointNode.getY());
			if (ContextCreator.sDebug) {System.out.println("CLIENT : Spawn at "
			+ getCurrentTime() + " on "+ iClientAgent.getNodeMap().get(NodeClientType.beginNode).getPlaceName());}
			
			/* Begin Method 2 */
			if (ContextCreator.sVersion2) {
				spawClientAgent(iClientAgent);
			}
			/* End Method 2 */
		}
	}

	/*
	 * Delete the Client 
	 */
	public void removeClientAgent(ClientAgent iClientAgent) {
		try {
			mClientAgents.remove(iClientAgent);
			mContext.remove(iClientAgent);
			FileTools.writeClientDates(iClientAgent);
			iClientAgent = null;
			if (ContextCreator.sDebug) {System.out.println("CLIENT : Client is remove");}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Delete the Taxi 
	 */
	public void removeTaxiAgent(TaxiAgent iTaxiAgent) {
		try {
			iTaxiAgent.getClientAgentsInTaxi().clear();
			mTaxiAgents.remove(iTaxiAgent);
			mContext.remove(iTaxiAgent);
			iTaxiAgent.getIntegerMap().put(IntegerTaxiType.backhomeDate, getCurrentTime());
			FileTools.writeTaxiDates(iTaxiAgent);
			iTaxiAgent = null;
			if (ContextCreator.sDebug) {System.out.println("TAXI : Taxi is remove");}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}