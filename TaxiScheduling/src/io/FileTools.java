package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import agents.ClientAgent;
import agents.TaxiAgent;
import au.com.bytecode.opencsv.CSVReader;
import data.DataBase;
import data.Type.IntegerClientType;
import data.Type.IntegerTaxiType;
import data.Type.NodeClientType;

public class FileTools {

	public static void readGraphNodes(File iGraphNodeFile)
			throws FileNotFoundException, IOException {
		try (CSVReader lCsvReader = new CSVReader(
				new FileReader(iGraphNodeFile), ';', '"', 1)) {
			List<String[]> lLines = lCsvReader.readAll();
			for (String[] lLine : lLines) {
				DataBase.getInstance().addNode(lLine[0],
						Integer.parseInt(lLine[1]), Integer.parseInt(lLine[2]));
			}
		}
	}

	public static void readGraphEdges(File iGraphFile)
			throws FileNotFoundException, IOException {
		try (CSVReader lCsvReader = new CSVReader(new FileReader(iGraphFile),
				';', '"', 1)) {
			List<String[]> lLines = lCsvReader.readAll();
			for (String[] lLine : lLines) {
				DataBase.getInstance().addEdge(lLine[0], lLine[1],
						Integer.parseInt(lLine[2]));
			}
		}
	}

	public static void readClients(File iClientFile)
			throws FileNotFoundException, IOException {
		try (CSVReader lCsvReader = new CSVReader(new FileReader(iClientFile),
				';', '"', 1)) {
			List<String[]> lLines = lCsvReader.readAll();
			for (String[] lLine : lLines) {
				DataBase.getInstance().addClientInStock(
						Integer.parseInt(lLine[4]), Integer.parseInt(lLine[1]),
						Integer.parseInt(lLine[3]), lLine[0], lLine[2]);
			}
		}
	}

	public static void writeClientDates(ClientAgent iClientAgent) throws IOException {
		int lSpawnDate = iClientAgent.getIntegerMap().get(IntegerClientType.spawnDate);
		int lBeginDate = iClientAgent.getIntegerMap().get(IntegerClientType.beginDate);
		int lPickUpDate = iClientAgent.getIntegerMap().get(IntegerClientType.pickupDate);
		int lDropDate = iClientAgent.getIntegerMap().get(IntegerClientType.dropDate);
		int lEndDate = iClientAgent.getIntegerMap().get(IntegerClientType.endDate);
		int lWaitingOut = lPickUpDate - lBeginDate;
		int lWaitingIn = lDropDate - lPickUpDate;
		String lPickUpNode = iClientAgent.getNodeMap().get(NodeClientType.pickupNode).getPlaceName();
		String lDropNode = iClientAgent.getNodeMap().get(NodeClientType.dropNode).getPlaceName();
		String lLogs = System.lineSeparator() + lSpawnDate + ";" + lBeginDate
				+ ";" + lPickUpDate + ";" + lDropDate + ";" + lEndDate + ";"
				+ lWaitingOut + ";" + lWaitingIn + ";" + lPickUpNode + ";" + lDropNode;
		File lFile = new File("output/logs-clients.csv");
		FileUtils.writeStringToFile(lFile, lLogs, true);
	}

	public static void writeTaxiDates(TaxiAgent iTaxiAgent) throws IOException {
		int lSpawnDate = iTaxiAgent.getIntegerMap().get(IntegerTaxiType.spawnDate);
		int lBackHomeDate = iTaxiAgent.getIntegerMap().get(IntegerTaxiType.backhomeDate);
		int lNbClients = iTaxiAgent.getIntegerMap().get(IntegerTaxiType.nbClients);
		int lNbMaxClients = iTaxiAgent.getIntegerMap().get(IntegerTaxiType.nbMaxClients);
		int lService = lBackHomeDate - lSpawnDate;
		String lLogs = System.lineSeparator() + lSpawnDate + ";"
				+ lBackHomeDate + ";" + lService + ";" + lNbClients + ";" + lNbMaxClients;
		File lFile = new File("output/logs-taxis.csv");
		FileUtils.writeStringToFile(lFile, lLogs, true);
	}

	public static void clearOutput() throws IOException {
		String lTitleClients = "Spawn;Begin;Pickup;Drop;End;WaitOut;WaitInTaxi;PickupNode;DropNode";
		String lTitleTaxis = "Spawn;End;TimeService;TotalClient;MaxClient";
		File lFileClients = new File("output/logs-clients.csv");
		File lFileTaxis = new File("output/logs-taxis.csv");
		FileUtils.writeStringToFile(lFileClients, lTitleClients, false);
		FileUtils.writeStringToFile(lFileTaxis, lTitleTaxis, false);
	}
}
