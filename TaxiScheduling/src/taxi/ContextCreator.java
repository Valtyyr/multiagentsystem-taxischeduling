package taxi;

import io.FileTools;

import java.io.File;
import java.io.IOException;

import repast.simphony.context.Context;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.SimpleGridAdder;
import repast.simphony.space.grid.WrapAroundBorders;
import data.DataBase;
import environment.AEnvironment;
import environment.Scheduler;
public class ContextCreator implements ContextBuilder<AEnvironment> {

	public static int sNbMaxClientInApp;
	public static int sNbMaxClientInTaxi;
	public static int sTimeService;
	public static boolean sDebug;
	public static boolean sRandom;
	public static boolean sVersion2;
	public static boolean sVersion3;
	
	/*
	 * EntryPoint of the application
	 * Authors : Valentin Macheret -- Mickael Tavares -- Axel Beneult Lefaucheux
	 * SCIA 2015
	 */
	@Override
	public Context<AEnvironment> build(Context<AEnvironment> iContext) {
		iContext.setId("Taxi");
		
		sNbMaxClientInApp = RunEnvironment.getInstance().getParameters().getInteger("maxClient");;
		sNbMaxClientInTaxi = RunEnvironment.getInstance().getParameters().getInteger("maxClientInTaxi");;
		sTimeService = RunEnvironment.getInstance().getParameters().getInteger("timeService");
		sDebug = RunEnvironment.getInstance().getParameters().getBoolean("debugMode");
		sRandom = RunEnvironment.getInstance().getParameters().getBoolean("randomMode");
		sVersion2 = RunEnvironment.getInstance().getParameters().getBoolean("version2");
		sVersion3 = RunEnvironment.getInstance().getParameters().getBoolean("version3");
		
		/* 
		 * Network is used for the graph logic 
		 */
		NetworkBuilder<AEnvironment> lBuilder = new NetworkBuilder<AEnvironment>("network", iContext, true);  		
		Network<AEnvironment> lNetwork = lBuilder.buildNetwork();

		/*
		 * Grid is used for the graph position 
		 */
		int lWidth = RunEnvironment.getInstance().getParameters().getInteger("gridWidth");
		int lHeight = RunEnvironment.getInstance().getParameters().getInteger("gridHeight");
		GridFactory mGridFactory = GridFactoryFinder.createGridFactory(null);
		Grid<AEnvironment> lGrid = mGridFactory.createGrid("grid", iContext,
				new GridBuilderParameters<AEnvironment>(new WrapAroundBorders(),
						new SimpleGridAdder<AEnvironment>(), true, lWidth, lHeight));

		/*
		 * DataBase initialization 
		 */
		DataBase.getInstance().init(iContext, lGrid, lNetwork);
		
		/*
		 * Creation of the network
		 * Add nodes, edges and stock clients in DataBase
		 */
		initNetWork();
		
		/*
		 * Creation of the Scheduler
		 */
		Scheduler lSchedulerAgent = new Scheduler(lNetwork, lGrid);
		iContext.add(lSchedulerAgent);
		
		return iContext;
	}

	private void initNetWork() {
		try {
			FileTools.clearOutput();
			FileTools.readGraphNodes(new File("resources/init-nodes.csv"));
			FileTools.readGraphEdges(new File("resources/init-edges.csv"));
			FileTools.readClients(new File("resources/init-clients.csv"));
			DataBase.getInstance().setTaxiBase(DataBase.getInstance().getGraph().getNodes().get(0));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
