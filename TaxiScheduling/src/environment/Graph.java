package environment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Stack;

import taxi.ContextCreator;
import agents.ClientAgent;
import agents.TaxiAgent;
import data.Type.BooleanClientType;
import data.Type.IntegerClientType;
import data.Type.NodeClientType;

public class Graph {

	private final List<Node> mNodes;
	
	public Graph() {
		mNodes = new ArrayList<Node>();
	}
	
	public List<Node> getNodes() {
		return mNodes;
	}
	
	/*
	 * Looking for best path in order
	 * to optimized the Client collect
	 * More info in the algorithm
	 */
	public Stack<Node> newOptimizedPath(Node iCurrentTaxiNode, 
			List<ClientAgent> iClientsInTaxi, 
			List<ClientAgent> iAllClients,
			int iCurrentTime,
			TaxiAgent iTaxi) {
		Stack<Node> lStack = new Stack<Node>();
		ClientAgent lAimedClient = null;
		Node lAimedNode = null;

		if (ContextCreator.sDebug) {System.out.println("TAXI : Is looking for new path....");}
		
		/*
		 * Looking for a Client that the Taxi
		 * can pick up
		 * Must be compliance with other Client restrictions
		 */
		if (iClientsInTaxi.size() < ContextCreator.sNbMaxClientInTaxi) {
			for (ClientAgent lClientAgent : iAllClients) {
				if (!iClientsInTaxi.contains(lClientAgent) && lClientAgent.isDisponible()) {
					int lDurationToBeginNewClient = calculateDuration(getDijkstra(iCurrentTaxiNode,
																				  lClientAgent.getNodeMap().get(NodeClientType.beginNode)));
					int lDurationBeginNewToEndNew = calculateDuration(getDijkstra(lClientAgent.getNodeMap().get(NodeClientType.beginNode),
																				  lClientAgent.getNodeMap().get(NodeClientType.endNode)));
					int lBeginDateNewClient = lClientAgent.getIntegerMap().get(IntegerClientType.beginDate);
					int lEndDateNewClient = lClientAgent.getIntegerMap().get(IntegerClientType.endDate);
					int lComplianceClientInTaxi = 0;
					
					for (ClientAgent lClientInTaxi : iClientsInTaxi) {
						int lDurationBeginNewToEndInTaxi = calculateDuration(getDijkstra(lClientAgent.getNodeMap().get(NodeClientType.beginNode),
																	  					 lClientInTaxi.getNodeMap().get(NodeClientType.endNode)));
						int lDurationEndNewToEndInTaxi = calculateDuration(getDijkstra(lClientAgent.getNodeMap().get(NodeClientType.endNode),
			  					 													   lClientInTaxi.getNodeMap().get(NodeClientType.endNode)));
						int lDurationEndInTaxiToEndNew = calculateDuration(getDijkstra(lClientInTaxi.getNodeMap().get(NodeClientType.endNode),
																					   lClientAgent.getNodeMap().get(NodeClientType.endNode)));
						int lEndDateClientInTaxi = lClientInTaxi.getIntegerMap().get(IntegerClientType.endDate);
	
						if (iCurrentTime > lBeginDateNewClient && lEndDateClientInTaxi > lEndDateNewClient) {
							if (iCurrentTime + lDurationToBeginNewClient + lDurationBeginNewToEndNew < lEndDateNewClient - ContextCreator.sTimeService
									&& iCurrentTime + lDurationToBeginNewClient + lDurationBeginNewToEndNew + lDurationEndNewToEndInTaxi < lEndDateClientInTaxi  - ContextCreator.sTimeService) {
								lComplianceClientInTaxi++;
								if (ContextCreator.sDebug) {System.out.println("CLIENT : "
								+ lClientAgent.getNodeMap().get(NodeClientType.beginNode).getPlaceName() + " ------> " 
								+ lClientAgent.getNodeMap().get(NodeClientType.endNode).getPlaceName() + " compliant with a new Client");}
							}
						}
						else if (iCurrentTime > lBeginDateNewClient && lEndDateClientInTaxi < lEndDateNewClient) {
							if (iCurrentTime + lDurationToBeginNewClient + lDurationBeginNewToEndInTaxi < lEndDateClientInTaxi - ContextCreator.sTimeService
									&& iCurrentTime + lDurationToBeginNewClient + lDurationBeginNewToEndInTaxi + lDurationEndInTaxiToEndNew < lEndDateNewClient - ContextCreator.sTimeService) {
								lComplianceClientInTaxi++;
								if (ContextCreator.sDebug) {System.out.println("CLIENT : "
								+ lClientAgent.getNodeMap().get(NodeClientType.beginNode).getPlaceName() + " ------> " 
								+ lClientAgent.getNodeMap().get(NodeClientType.endNode).getPlaceName() + " compliant with a new Client");}
							}
						}
					/*else if (iCurrentTime < lBeginDateNewClient && lEndDateClientInTaxi > lEndDateNewClient) {
							if (iCurrentTime + lDurationToBeginNewClient > lBeginDateNewClient
								&& lBeginDateNewClient + lDurationBeginNewToEndNew < lEndDateNewClient
								&& lBeginDateNewClient + lDurationBeginNewToEndNew + lDurationEndNewToEndInTaxi < lEndDateClientInTaxi) {
								lComplianceClientInTaxi++;
								if (DataBase.getInstance().mDebug) {System.out.println("CLIENT : "
								+ lClientAgent.getNodeMap().get(NodeClientType.beginNode).getPlaceName() + " ------> " 
								+ lClientAgent.getNodeMap().get(NodeClientType.endNode).getPlaceName() + " compliant with a new Client");}
							}
						}
						else if (iCurrentTime < lBeginDateNewClient && lEndDateClientInTaxi < lEndDateNewClient) {
							if (iCurrentTime + lDurationToBeginNewClient > lBeginDateNewClient
									&& lBeginDateNewClient + lDurationBeginNewToEndInTaxi < lEndDateClientInTaxi
									&& lBeginDateNewClient + lDurationBeginNewToEndInTaxi + lDurationEndInTaxiToEndNew < lEndDateNewClient) {
								lComplianceClientInTaxi++;
								if (DataBase.getInstance().mDebug) {System.out.println("CLIENT : "
								+ lClientAgent.getNodeMap().get(NodeClientType.beginNode).getPlaceName() + " ------> " 
								+ lClientAgent.getNodeMap().get(NodeClientType.endNode).getPlaceName() + " compliant with a new Client");}
							}
						} else {
							break;
						}*/
						
					}
					if (lComplianceClientInTaxi == iClientsInTaxi.size()) {
						if (ContextCreator.sDebug) {System.out.println("TAXI : New client aimed : "
								+ lClientAgent.getNodeMap().get(NodeClientType.beginNode).getPlaceName()
								+ " --------> "
								+ lClientAgent.getNodeMap().get(NodeClientType.endNode).getPlaceName());}
						lClientAgent.getBooleanMap().put(BooleanClientType.aimed, true);
						lAimedNode = lClientAgent.getNodeMap().get(NodeClientType.beginNode);
						lAimedClient = lClientAgent;
						break;
					}
				}
			}
		}
		
		/*
		 * If there is no compliance
		 * Looking for a node destination
		 */
		if (lAimedNode == null) {
			int lMinDate = Integer.MAX_VALUE;
			for (ClientAgent lClientAgent : iClientsInTaxi) {
				int lEndDate = lClientAgent.getIntegerMap().get(IntegerClientType.endDate);
				if (lEndDate < lMinDate) {
					lMinDate = lEndDate;
					lAimedNode = lClientAgent.getNodeMap().get(NodeClientType.endNode);
					lAimedClient = lClientAgent;
				}
			}
			if (ContextCreator.sDebug) {System.out.println("TAXI : Didn't find new Client");}
		}

		if (ContextCreator.sDebug) {System.out.println("TAXI : Considering new path from "
				+ iCurrentTaxiNode.getPlaceName() + " to "
				+ lAimedNode.getPlaceName());}
		
		List<Node> lPath = getDijkstra(iCurrentTaxiNode, lAimedNode);
		lStack.add(lAimedNode);
		lStack.addAll(lPath);
		
		iTaxi.mAimedClient = lAimedClient;
		
		return lStack;
	}
	
	public Node getNode(String iName) {
		Node lFindNode = null;
		for (Node lNode : mNodes) {
			if (iName.equals(lNode.getPlaceName())) {
				lFindNode = lNode;
			}
		}
		return lFindNode;
	}
	
	public int calculateDuration(List<Node> iPath) {
		int lDuration =  ContextCreator.sTimeService * 2;
		for (int i = 0; i < iPath.size() - 1; ++i) {
			lDuration += iPath.get(i).getAdjacentNodes().get(iPath.get(i + 1));
		}
		return lDuration;
	}
	
	public List<Node> getDijkstra(Node iSourceNode, Node iDestinationNode) {
		reset();
		computePaths(iSourceNode);
		return shortestPathTo(iSourceNode, iDestinationNode);
	}
	
	private void computePaths(Node iSourceNode) {
		iSourceNode.mMinDistance = 0.;
		PriorityQueue<Node> lNodeQueue = new PriorityQueue<Node>();
		lNodeQueue.add(iSourceNode);

		while (!lNodeQueue.isEmpty()) {
			Node lCurrentNode = lNodeQueue.poll();
			for (Entry<Node, Integer> lEntry : lCurrentNode.getAdjacentNodes().entrySet()) {
				Node lAdjacentNode = lEntry.getKey();
				double lWeight = lEntry.getValue();
				double lDistanceNode = lCurrentNode.mMinDistance + lWeight;
				if (lDistanceNode < lAdjacentNode.mMinDistance) {
				    lNodeQueue.remove(lAdjacentNode);
				    lAdjacentNode.mMinDistance = lDistanceNode;
				    lAdjacentNode.mPrievousNode = lCurrentNode;
				    lNodeQueue.add(lAdjacentNode);
				}
			}
		}
	}

	private List<Node> shortestPathTo(Node iSourceNode, Node iDestinationNode) {
		List<Node> lPath = new ArrayList<Node>();
		for (Node lNode = iDestinationNode; lNode != null; lNode = lNode.mPrievousNode) {
			lPath.add(lNode);
	     }
		Collections.reverse(lPath);
		
		return lPath;
	}
	
	private void reset() {
		for (Node lNode : mNodes) {
			lNode.mMinDistance = Double.POSITIVE_INFINITY;
			lNode.mPrievousNode = null;
		}
	}
}
