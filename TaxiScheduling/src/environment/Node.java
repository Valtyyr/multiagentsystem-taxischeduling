package environment;

import java.util.HashMap;
import java.util.Map;

import data.Type.BooleanNodeType;


public class Node extends AEnvironment implements Comparable<Node> {

	private final Map<Node, Integer> mAdjacentNodes;
	private final Map<BooleanNodeType, Boolean> mBooleanMap;
	private final String mName;
	
	public double mMinDistance = Double.POSITIVE_INFINITY;
	public Node mPrievousNode;
	
	public Node(String iName) {
		mAdjacentNodes = new HashMap<Node, Integer>();
		mName = iName;
		mBooleanMap = new HashMap<BooleanNodeType, Boolean>();
		mBooleanMap.put(BooleanNodeType.isDest, false);
	}

	public void addAdjacentNode(Node iNode, int iWeight) {
		mAdjacentNodes.put(iNode, iWeight);
	}
	
	public Map<Node, Integer> getAdjacentNodes() {
		return mAdjacentNodes;
	}
	
	public Map<BooleanNodeType, Boolean> getBooleanMap() {
		return mBooleanMap;
	}
	
	public String getPlaceName() {
		return mName;
	}

	public int compareTo(Node iOtherNode) {
        return Double.compare(mMinDistance, iOtherNode.mMinDistance);
    }
}
