package environment;

import data.DataBase;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import taxi.ContextCreator;

public class Scheduler extends AEnvironment {

	public Scheduler(Network<AEnvironment> iNetwork,
			Grid<AEnvironment> iGrid) {
	}

	@ScheduledMethod(start = 1, interval = 1, priority = 2)
	public void compute() {
		if (!ContextCreator.sRandom) {DataBase.getInstance().updateClientInContextSchedule();}
		if (ContextCreator.sRandom) {DataBase.getInstance().updateClientInContextRandom();}
	}
	
	@ScheduledMethod(start = 1, interval = 1, priority = 1)
	public void implement() {}
}
