package environment;

import java.awt.Color;

import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import data.Type.BooleanNodeType;

public class NodeStyle extends DefaultStyleOGL2D {

	@Override
	public Color getColor(Object o) {
		if (o instanceof Node) {
			if (((Node) o).getBooleanMap().get(BooleanNodeType.isDest)) {
				return Color.RED;
			}
		}
		return Color.GREEN;
	}

	@Override
	public String getLabel(Object object) {
		if (object instanceof Node) {
			return ((Node) object).getPlaceName();
		}
		return "poulet";
	}

	@Override
	public Color getLabelColor(Object object) {
		return Color.BLACK;
	}
	
	@Override
	public Color getBorderColor(Object object) {
		if (object instanceof Node) {
			return Color.BLACK;
		}
		return super.getBorderColor(object);
	}
}
