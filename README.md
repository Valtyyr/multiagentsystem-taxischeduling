# README #

### Authors ###

* Valentin 'Valtyr' Macheret
* Mickael Tavares

### Overview ###

Taxi scheduling simulation with multi-agent system on Repast Symphony (Java 7). It allow to see taxi scheduling through different ways.

### How do I get set up? ###

You need : 

* A Java Runtime Environment 7.
* A Java Software Development Kit.
* Repast Symphony (it includes Eclipse Kepler).

### High lines of implementation ###

This taxi scheduling is organized as follow :

* The city is represents with a graph datastructure.
* So Edges are streets.
* And Nodes are places where clients can spawn.
* These clients appear on one place at a specific time and want to be carry by taxi to a specific place at a specific time (at last).
* In order to bring those clients, taxis will appear in order to bring them at destination.
* With many different strategies, taxis will change their behavior.

### Run guidelines ###

#### Input ####

Many app input can be set by the user through CSV files :

* init-nodes.csv file establish manually where will be situate each node of the graph.
* init-edges.csv file establish manually relation between pair of nodes.
* init-clients.csv file establish manually where and when a client will spawn during the simulation.

#### Output ####

During the app execution, many csv output files will be generate in order to make trasability. They will be generate in the output directory.

#### Options ####

Many options can be set before run the application :

* You can choose which strategy you wan to apply.
* Max number of client in one taxi.
* Max number of taxi present in the same time.
* You can generate random clients instead of manual set clients.